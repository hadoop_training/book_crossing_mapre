package com.training.hadoop.sequencer.users;

import com.training.hadoop.common.UserWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UsersSequencerMapperTest {

    @Test
    void shouldMapLine() throws IOException, InterruptedException {
        String line = "\"49446\";\"trier, rheinland-pfalz, germany\";\"33\"";
        Mapper.Context context = Mockito.mock(Mapper.Context.class);

        new UsersSequencerMapper().map(null, new Text(line), context);

        ArgumentCaptor<LongWritable> idCaptor = ArgumentCaptor.forClass(LongWritable.class);
        ArgumentCaptor<UserWritable> userCaptor = ArgumentCaptor.forClass(UserWritable.class);
        Mockito.verify(context).write(idCaptor.capture(), userCaptor.capture());

        LongWritable id = idCaptor.getValue();
        UserWritable user = userCaptor.getValue();

        assertEquals(49446, id.get());
        assertEquals(49446, user.getId().get());
        assertEquals(33, user.getAge().get());
        assertEquals("trier, rheinland-pfalz, germany", user.getLocation().toString());
    }

    @Test
    void shouldMapComplicatedLine() throws IOException, InterruptedException {
        String line = "\"5262\";\"&#304;zmir, n/a, turkey\";\"27\"";
        Mapper.Context context = Mockito.mock(Mapper.Context.class);

        new UsersSequencerMapper().map(null, new Text(line), context);

        ArgumentCaptor<LongWritable> idCaptor = ArgumentCaptor.forClass(LongWritable.class);
        ArgumentCaptor<UserWritable> userCaptor = ArgumentCaptor.forClass(UserWritable.class);
        Mockito.verify(context).write(idCaptor.capture(), userCaptor.capture());

        LongWritable id = idCaptor.getValue();
        UserWritable user = userCaptor.getValue();

        assertEquals(5262, id.get());
        assertEquals(5262, user.getId().get());
        assertEquals(27, user.getAge().get());
        assertEquals("&#304;zmir, n/a, turkey", user.getLocation().toString());
    }

    @Test
    void shouldMapNullAge() throws IOException, InterruptedException {
        String line = "\"5262\";\"&#304;zmir, n/a, turkey\";NULL";
        Mapper.Context context = Mockito.mock(Mapper.Context.class);

        new UsersSequencerMapper().map(null, new Text(line), context);

        ArgumentCaptor<LongWritable> idCaptor = ArgumentCaptor.forClass(LongWritable.class);
        ArgumentCaptor<UserWritable> userCaptor = ArgumentCaptor.forClass(UserWritable.class);
        Mockito.verify(context).write(idCaptor.capture(), userCaptor.capture());

        LongWritable id = idCaptor.getValue();
        UserWritable user = userCaptor.getValue();

        assertEquals(5262, id.get());
        assertEquals(5262, user.getId().get());
        assertEquals(0, user.getAge().get());
        assertEquals("&#304;zmir, n/a, turkey", user.getLocation().toString());
    }

}