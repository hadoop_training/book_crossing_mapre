package com.training.hadoop.sequencer.books;

import com.training.hadoop.common.BookWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

class BooksSequencerMapperTest {

    @Test
    void shouldConvertCsvLineToBook() throws Exception {
        String line = "\"0830714014\";\"Always Daddy's Girl: Understanding Your Father's Impact on Who You Are\";\"H. Norman Wright\";\"1989\";\"Regal Books\";\"http://images.amazon.com/images/P/0830714014.01.THUMBZZZ.jpg\";\"http://images.amazon.com/images/P/0830714014.01.MZZZZZZZ.jpg\";\"http://images.amazon.com/images/P/0830714014.01.LZZZZZZZ.jpg\"";
        Mapper.Context context = Mockito.mock(Mapper.Context.class);

        new BooksSequencerMapper().map(null, new Text(line), context);

        ArgumentCaptor<Text> isbnCaptor = ArgumentCaptor.forClass(Text.class);
        ArgumentCaptor<BookWritable> expectedBookCaptor = ArgumentCaptor.forClass(BookWritable.class);
        verify(context).write(isbnCaptor.capture(), expectedBookCaptor.capture());

        Text isbn = isbnCaptor.getValue();
        BookWritable book = expectedBookCaptor.getValue();
        assertEquals("0830714014", isbn.toString());
        assertEquals("0830714014", book.getIsbn().toString());
        assertEquals("Always Daddy's Girl: Understanding Your Father's Impact on Who You Are", book.getTitle().toString());
        assertEquals("H. Norman Wright", book.getAuthor().toString());
        assertEquals("Regal Books", book.getPublisher().toString());
        assertEquals(1989, book.getYearOfPublication().get());
    }


    @Test
    void shouldConvertCsvLineWithTitleWithQutesToBook() throws Exception {
        String line = "\"0140292918\";\"Of Mice and Men (Steinbeck \\\"Essentials\\\")\";\"John Steinbeck\";\"2001\";\"Penguin Books Ltd\";\"http://images.amazon.com/images/P/0140292918.01.THUMBZZZ.jpg\";\"http://images.amazon.com/images/P/0140292918.01.MZZZZZZZ.jpg\";\"http://images.amazon.com/images/P/0140292918.01.LZZZZZZZ.jpg\"";
        Mapper.Context context = Mockito.mock(Mapper.Context.class);

        new BooksSequencerMapper().map(null, new Text(line), context);

        ArgumentCaptor<Text> isbnCaptor = ArgumentCaptor.forClass(Text.class);
        ArgumentCaptor<BookWritable> expectedBookCaptor = ArgumentCaptor.forClass(BookWritable.class);
        verify(context).write(isbnCaptor.capture(), expectedBookCaptor.capture());

        Text isbn = isbnCaptor.getValue();
        BookWritable book = expectedBookCaptor.getValue();
        assertEquals("0140292918", isbn.toString());
        assertEquals("0140292918", book.getIsbn().toString());
        assertEquals("Of Mice and Men (Steinbeck \\\"Essentials\\\")", book.getTitle().toString());
    }


}