package com.training.hadoop.sequencer.books;

import com.training.hadoop.common.BookWritable;
import com.training.hadoop.common.LineSplitter;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.log4j.Logger;

public class BooksSequencerMapper extends Mapper<LongWritable, Text, BookWritable, NullWritable> {

    private static Logger logger = Logger.getLogger(BooksSequencerMapper.class);

    @Override
    protected void map(LongWritable key, Text value, Context context) {
        String line = value.toString();

        if (line.contains("ISBN")) {
            return;
        }

        String[] split = LineSplitter.splitCSVLineWithQuotes(line);

        String isbn = split[0];

        try {
            context.write(toBook(split, isbn), NullWritable.get());
        } catch (Exception e) {
            Path filePath = ((FileSplit) context.getInputSplit()).getPath();
            logger.error("invalid line: " + line + " in file " + filePath.toString(), e);
        }
    }

    private BookWritable toBook(String[] split, String isbn) {
        String title = split[1];
        String author = split[2];
        int year = Integer.parseInt(split[3]);
        String publisher = split[4];
        BookWritable book = new BookWritable();
        book.setIsbn(isbn);
        book.setTitle(title);
        book.setAuthor(author);
        book.setYearOfPublication(year);
        book.setPublisher(publisher);
        return book;
    }
}
