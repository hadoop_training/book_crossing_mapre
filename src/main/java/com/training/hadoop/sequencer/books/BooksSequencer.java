package com.training.hadoop.sequencer.books;

import com.training.hadoop.common.BookWritable;
import com.training.hadoop.common.ConfiguredJob;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;


public class BooksSequencer extends ConfiguredJob {

    public static void main(String[] args) {
        try {
            int res = ToolRunner.run(new Configuration(), new BooksSequencer(), args);
            System.exit(res);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    protected void configureJob(Job job, Path input, Path output) throws IOException {
        job.setMapperClass(BooksSequencerMapper.class);
        job.setNumReduceTasks(0);
        job.setOutputKeyClass(BookWritable.class);
        job.setOutputValueClass(NullWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        TextInputFormat.addInputPath(job, new Path(input, "BX-Books.csv"));
        SequenceFileOutputFormat.setOutputPath(job, output);
    }

    @Override
    protected String getJobName() {
        return "books sequencer";
    }
}
