package com.training.hadoop.sequencer.ratings;

import com.training.hadoop.common.LineSplitter;
import com.training.hadoop.common.RatingWritable;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.log4j.Logger;

import java.io.IOException;

public class BooksRankingsSequencerMapper extends Mapper<LongWritable, Text, RatingWritable, NullWritable> {

    private static Logger logger = Logger.getLogger(BooksRankingsSequencerMapper.class);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        if (line.contains("User-ID")) {
            return;
        }

        String[] split = LineSplitter.splitCSVLineWithQuotes(line);

        try {
            int userId = Integer.parseInt(split[0]);
            String isbn = split[1];
            int rating = Integer.parseInt(split[2]);
            RatingWritable ratingWritable = new RatingWritable();
            ratingWritable.setIsbn(isbn);
            ratingWritable.setRating(rating);
            ratingWritable.setUserId(userId);

            context.write(ratingWritable, NullWritable.get());
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            Path filePath = ((FileSplit) context.getInputSplit()).getPath();
            logger.error("invalid line: " + line + " in file " + filePath.toString(), e);
        }
    }
}
