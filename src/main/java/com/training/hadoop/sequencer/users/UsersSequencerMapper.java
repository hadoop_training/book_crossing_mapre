package com.training.hadoop.sequencer.users;

import com.training.hadoop.common.LineSplitter;
import com.training.hadoop.common.UserWritable;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.log4j.Logger;

import java.io.IOException;

public class UsersSequencerMapper extends Mapper<LongWritable, Text, UserWritable, NullWritable> {

    private static Logger logger = Logger.getLogger(UsersSequencerMapper.class);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        if (line.contains("User-ID")) {
            return;
        }

        if (line.contains("NULL")) {
            line = line.replaceAll("NULL", "");
        }

        String[] split = LineSplitter.splitCSVLineWithQuotes(line);

        try {
            String userIdString = split[0].replaceAll("\"", "");
            String location = split[1].replaceAll("\"", "");
            int age = getAge(split);
            long userId = Long.parseLong(userIdString.replaceAll("\"", ""));
            UserWritable user = new UserWritable();
            user.setId(userId);
            user.setAge(age);
            user.setLocation(location);
            context.write(user, NullWritable.get());
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            Path filePath = ((FileSplit) context.getInputSplit()).getPath();
            logger.error("invalid line: " + line + " in file " + filePath.toString());
        }
    }

    private int getAge(String[] split) {
        if (split.length < 3) {
            return 0;
        }
        return Integer.parseInt(split[2].replaceAll("\"", ""));
    }
}
