package com.training.hadoop.sequencer.users;

import com.training.hadoop.common.ConfiguredJob;
import com.training.hadoop.common.UserWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;


public class UserSequencer extends ConfiguredJob {

    public static void main(String[] args) {
        try {
            int res = ToolRunner.run(new Configuration(), new UserSequencer(), args);
            System.exit(res);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    protected void configureJob(Job job, Path input, Path output) throws IOException {
        job.setMapperClass(UsersSequencerMapper.class);
        job.setNumReduceTasks(0);
        job.setOutputKeyClass(UserWritable.class);
        job.setOutputValueClass(NullWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        TextInputFormat.addInputPath(job, new Path(input, "BX-Users.csv"));
        SequenceFileOutputFormat.setOutputPath(job, output);
    }

    @Override
    protected String getJobName() {
        return "users sequencer";
    }
}
