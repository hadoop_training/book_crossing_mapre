package com.training.hadoop.sequencer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class SequenceFileReader extends Configured implements Tool {

    public static void main(String[] args) {
        try {
            int res = ToolRunner.run(new Configuration(), new SequenceFileReader(), args);
            System.exit(res);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration config = getConf();

        Path input = new Path(args[0]);

        SequenceFile.Reader reader = new SequenceFile.Reader(config, SequenceFile.Reader.file(input));

        Writable key = createKeyObject(reader);
        Writable value = createValueObject(reader);

        while (reader.next(key, value)) {
            System.out.println(String.format("%s = %s", key, value));
        }
        reader.close();
        return 0;
    }

    private Writable createValueObject(SequenceFile.Reader reader) throws InstantiationException, IllegalAccessException {
        return createWritableFrom(reader.getValueClass());
    }

    private Writable createKeyObject(SequenceFile.Reader reader) throws InstantiationException, IllegalAccessException {
        return createWritableFrom(reader.getKeyClass());
    }

    private Writable createWritableFrom(Class<?> theClass) throws InstantiationException, IllegalAccessException {
        if (theClass.equals(NullWritable.class)) {
            return NullWritable.get();
        }
        return (Writable) theClass.newInstance();
    }
}
