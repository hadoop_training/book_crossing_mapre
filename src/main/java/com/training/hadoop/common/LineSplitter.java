package com.training.hadoop.common;

public class LineSplitter {

    public static String[] splitCSVLineWithQuotes(String line) {
        String stringValue = line.substring(1, line.length() - 1);
        String[] split = stringValue.split("\";\"");
        return split;
    }

}
