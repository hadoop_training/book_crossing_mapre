package com.training.hadoop.common;

import org.apache.hadoop.io.*;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class UserWritable implements Writable, WritableComparable<UserWritable> {

    private LongWritable id = new LongWritable();
    private Text location = new Text();
    private IntWritable age = new IntWritable();

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        id.write(dataOutput);
        location.write(dataOutput);
        age.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        id.readFields(dataInput);
        location.readFields(dataInput);
        age.readFields(dataInput);
    }


    @Override
    public int compareTo(UserWritable o) {
        return this.id.compareTo(o.id);
    }

    public LongWritable getId() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public Text getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location.set(location);
    }

    public IntWritable getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    @Override
    public String toString() {
        return "UserWritable{" +
                "id=" + id +
                ", location=" + location +
                ", age=" + age +
                '}';
    }
}
