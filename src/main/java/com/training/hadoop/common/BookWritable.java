package com.training.hadoop.common;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class BookWritable implements Writable, WritableComparable<BookWritable> {

    private Text title = new Text();
    private Text author = new Text();
    private Text isbn = new Text();
    private Text publisher = new Text();
    private IntWritable yearOfPublication = new IntWritable();

    @Override
    public int compareTo(BookWritable o) {
        return this.isbn.compareTo(o.isbn);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        title.write(dataOutput);
        author.write(dataOutput);
        isbn.write(dataOutput);
        publisher.write(dataOutput);
        yearOfPublication.write(dataOutput);
    }

    @Override
    public void readFields(DataInput input) throws IOException {
        title.readFields(input);
        author.readFields(input);
        isbn.readFields(input);
        publisher.readFields(input);
        yearOfPublication.readFields(input);
    }

    public Text getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public Text getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author.set(author);
    }

    public Text getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn.set(isbn);
    }

    public Text getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher.set(publisher);
    }

    public IntWritable getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication.set(yearOfPublication);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BookWritable that = (BookWritable) o;

        return new EqualsBuilder()
                .append(title, that.title)
                .append(author, that.author)
                .append(isbn, that.isbn)
                .append(publisher, that.publisher)
                .append(yearOfPublication, that.yearOfPublication)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(title)
                .append(author)
                .append(isbn)
                .append(publisher)
                .append(yearOfPublication)
                .toHashCode();
    }


    @Override
    public String toString() {
        return "BookWritable{" +
                "title=" + title +
                ", author=" + author +
                ", isbn=" + isbn +
                ", publisher=" + publisher +
                ", yearOfPublication=" + yearOfPublication +
                '}';
    }
}
