package com.training.hadoop.common;

import org.apache.hadoop.io.*;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Comparator;

public class RatingWritable implements Writable, WritableComparable<RatingWritable> {

    private LongWritable userId = new LongWritable();
    private Text isbn = new Text();
    private IntWritable rating = new IntWritable();

    @Override
    public int compareTo(RatingWritable o) {
        return Comparator.comparing(RatingWritable::getUserId)
                .thenComparing(RatingWritable::getIsbn)
                .thenComparing(RatingWritable::getRating)
                .compare(this, o);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        userId.write(dataOutput);
        isbn.write(dataOutput);
        rating.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        userId.readFields(dataInput);
        isbn.readFields(dataInput);
        rating.readFields(dataInput);
    }

    public LongWritable getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId.set(userId);
    }

    public Text getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn.set(isbn);
    }

    public IntWritable getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating.set(rating);
    }

    @Override
    public String toString() {
        return "RatingWritable{" +
                "userId=" + userId +
                ", isbn=" + isbn +
                ", rating=" + rating +
                '}';
    }
}
