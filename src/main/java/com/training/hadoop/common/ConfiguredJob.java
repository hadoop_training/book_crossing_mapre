package com.training.hadoop.common;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;

import java.io.IOException;

public abstract class ConfiguredJob extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Path input = new Path(args[0]);
        Path output = new Path(args[1]);
        FileSystem fileSystem = FileSystem.get(conf);
        if (fileSystem.exists(output)) {
            fileSystem.delete(output, true);
        }
        Job job = createJob(conf, input, output);
        job.waitForCompletion(true);
        return 0;
    }

    protected Job createJob(Configuration configuration, Path input, Path output) throws IOException {
        Job job = Job.getInstance(configuration, getJobName());
        job.setJarByClass(getClass());
        configureJob(job, input, output);
        return job;
    }

    protected abstract void configureJob(Job job, Path input, Path output) throws IOException;

    protected abstract String getJobName();

}
