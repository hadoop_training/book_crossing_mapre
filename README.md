## budowanie
```bash
mvn clean compile assembly:single
```
Plikiem wynikowym jest `bookcross-final.jar` w katalogu `target`.

## przygotowanie danych

 - pobranie i rozpakowanie danych (`http://www2.informatik.uni-freiburg.de/~cziegler/BX/`)
 - stworzenie katalogów w HDFS `hdfs dfs -mkdir -p /data/plain/books`
 - skopiowanie danych do HDFS `hdfs dfs -put BX-CSV-Dump/* /data/plain/books`
 - sprawdznie `hdfs dfs -ls /data/plain/books`

## zadania
 - [zmiana formatu plików](docs/001.md)
